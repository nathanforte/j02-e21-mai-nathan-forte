#include <iostream>
using namespace std;

#include <SDL.h>

//	size of window on screen
constexpr auto WIDTH = 1000;
constexpr auto HEIGHT = 700;

//	include desired libraries if needed
#include "../lib_Point/Point.h"
#include "../lib_Slider/Slider.h"

//	entry point of application
int main(int argc, char** argv) {

#pragma region SDL initialization
	// SDL initialization
	SDL_Window* fenetre = NULL;
	SDL_Renderer* renderer = NULL;

	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		cout << "ERREUR : can't initialize SDL!" << endl;
		exit(0);
	}

	//SDL_ShowCursor(SDL_DISABLE);	//	hide mouse cursor
	SDL_ShowCursor(SDL_ENABLE);	//	show mouse cursor

	//	create the window and its renderer
	fenetre = SDL_CreateWindow("SDL template", 200, 100, WIDTH, HEIGHT, 0);
	renderer = SDL_CreateRenderer(fenetre, 0, 0);
#pragma endregion

	//	prepare usefull objects here
	//	****************************
	/*SDL_Rect rectangle[6];
		for (int i = 0; i < 6; i++) {
			rectangle[i].x = 50;
			rectangle[i].y = 50;
			rectangle[i].w = 50;
			rectangle[i].h = 50;
	}*/
	SDL_Rect rectangle1;
	rectangle1.x = 50;
	rectangle1.y = 50;
	rectangle1.w = 50;
	rectangle1.h = 50;
	

	//	main loop here
	//	**************
	while (true) {
		//	draw image
		//	**********

		
		

		//	- clear window
		SDL_SetRenderDrawColor(renderer, 5, 0, 0, SDL_ALPHA_OPAQUE);
		SDL_RenderClear(renderer);

		//	- draw any desired graphical object
		/*SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
		SDL_RenderFillRect(renderer, &rectangle[1]);
		SDL_RenderFillRect(renderer, &rectangle[2]);
		SDL_RenderFillRect(renderer, &rectangle[3]);
		SDL_RenderFillRect(renderer, &rectangle[4]);
		SDL_RenderFillRect(renderer, &rectangle[5]);
		SDL_RenderFillRect(renderer, &rectangle[6]);*/
		/*for (int i - 0; i < 6; i++) {
			rectang
		}*/

		//	event management
		//	****************

		//	remove next event from queue
		SDL_Event event;
		SDL_PollEvent(&event);

		//	give event to objects for update

		

		//	show rendering buffer
		//	*********************
		

		//	check keypress for exit
		//	***************************
		if (event.type == SDL_KEYDOWN) {
			break;
		}
	}

#pragma region SDL quit
	//	destroy window and quit SDL
	SDL_DestroyWindow(fenetre);
	SDL_Quit();
#pragma endregion

	return 0;
}