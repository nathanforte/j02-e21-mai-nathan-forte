#pragma once

#include "../lib_Point/Point.h"

class GameObject {
private:
	

	Point position;
	Vector speed;		//	in pixels per second

	__time64_t lastUpdate;	//	date in nanoseconds since last update

	int width, height;	//	limit of box

	__time64_t getTimeInNanoSeconds();
	void update();

public:
	//	constructor
	GameObject(Point position, Vector speed, int width, int height);

	//	getters and setters
	Point getPosition();
	Vector getSpeed();
	void setSpeed(Vector newSpeed);

	void draw(SDL_Renderer* renderer, Color color);
};

