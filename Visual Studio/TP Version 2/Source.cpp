#include <iostream>
using namespace std;

#include <SDL.h>

//	size of window on screen
constexpr auto WIDTH = 600;
constexpr auto HEIGHT = 400;

//	include desired libraries if needed
#include "../lib_Point/Point.h"
#include "../lib_Slider/Slider.h"
#include "Windows.h"
#include "GameObject.h"


//	entry point of application
int main(int argc, char** argv) {

#pragma region SDL initialization
	// SDL initialization
	SDL_Window* fenetre = NULL;
	SDL_Renderer* renderer = NULL;

	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		cout << "ERREUR : can't initialize SDL!" << endl;
		exit(0);
	}

	//SDL_ShowCursor(SDL_DISABLE);	//	hide mouse cursor
	SDL_ShowCursor(SDL_ENABLE);	//	show mouse cursor

	//	create the window and its renderer
	fenetre = SDL_CreateWindow("SDL template", 200, 100, WIDTH, HEIGHT, 0);
	renderer = SDL_CreateRenderer(fenetre, 0, 0);
#pragma endregion

	//	prepare usefull objects here
	//	****************************
	
	GameObject Balle(0, Point (300, 330), Vector (-100, 100), 100, 100);

	//	main loop here
	//	**************
	while (true) {
		// pallette de jeu avec d�tection de la position y du curseur

		POINT cursorPos;
		GetCursorPos(&cursorPos);
		SDL_Rect palletteDeJeu;
		palletteDeJeu.x = 50;
		palletteDeJeu.y = cursorPos.y;
		palletteDeJeu.w = 10;
		palletteDeJeu.h = 100;
		
		//cr�ation de la cible

		SDL_Rect cibleDeJeu;
		cibleDeJeu.x = 500;
		cibleDeJeu.y = 150;
		cibleDeJeu.w = 50;
		cibleDeJeu.h = 70;

		//	draw image
		//	**********

		//	- clear window
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
		SDL_RenderClear(renderer);

		//	- draw any desired graphical object
		SDL_SetRenderDrawColor(renderer, 0, 255, 0, SDL_ALPHA_OPAQUE);
		SDL_RenderFillRect(renderer, &palletteDeJeu);

		SDL_SetRenderDrawColor(renderer, 255, 0, 0, SDL_ALPHA_OPAQUE);
		SDL_RenderFillRect(renderer, &cibleDeJeu);

		Balle.draw(renderer, Color(0, 0, 255, SDL_ALPHA_OPAQUE)); 
	
		//	event management
		//	****************

		//compteur de points

		//int nbPoints = 0;
		//cout << "Points: " << nbPoints;

		//	remove next event from queue
		SDL_Event event;
		SDL_PollEvent(&event);

		//	give event to objects for update
		
	
		//	show rendering buffer
		//	*********************
		SDL_RenderPresent(renderer);

		//	check keypress for exit
		//	***************************
		if (event.type == SDL_KEYDOWN) {
			break;
		}
	}

#pragma region SDL quit
	//	destroy window and quit SDL
	SDL_DestroyWindow(fenetre);
	SDL_Quit();
#pragma endregion

	return 0;
}