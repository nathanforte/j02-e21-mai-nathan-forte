#include "GameObject.h"
#include <time.h>

__time64_t GameObject::getTimeInNanoSeconds()
{
	struct _timespec64 ts;
	if (_timespec64_get(&ts, TIME_UTC) == 0) {
		throw 0;
	}

	return ts.tv_sec * 1000000000 + ts.tv_nsec;
}

GameObject::GameObject(double mass, Point position, Vector speed, int width, int height) : position(position), speed(speed), acceleration(0, 0) {
	this->lastUpdate = this->getTimeInNanoSeconds();
	this->width = width;
	this->height = height;
}

void GameObject::update() {
	__time64_t currentUpdate = this->getTimeInNanoSeconds();

	//	compute elapsed time (in seconds) since last position change
	double deltaT = (currentUpdate - this->lastUpdate) / 1e9;

	this->lastUpdate = currentUpdate;

	//	compute new position of object now
	this->position.x = this->position.x + this->speed.x * deltaT;
	this->position.y = this->position.y + this->speed.y * deltaT;
	//	OR this->position = this->position + this->speed * deltaT;		//	using library operators
}

Point GameObject::getPosition() {
	return this->position;
}

Vector GameObject::getSpeed() {
	return this->speed;
}

void GameObject::setSpeed(Vector newSpeed) {
	this->speed = newSpeed;
}

void GameObject::draw(SDL_Renderer* renderer, Color color) {
	this->update();

	//	check borders of screen
	if ((this->position.x > width&& this->speed.x > 0) ||
		(this->position.x < 0 && this->speed.x < 0)) {
		this->speed.x *= -0.9;
	}
	if ((this->position.y > height&& this->speed.y > 0) ||
		(this->position.y < 0 && this->speed.y < 0)) {
		this->speed.y *= -0.9;
	}

	this->position.draw(renderer, color, 3);
	//this->position.drawCircle(renderer, 10, color, true);
}
